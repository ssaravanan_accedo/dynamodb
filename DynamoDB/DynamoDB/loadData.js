﻿var AWS = require("aws-sdk");
var fs = require('fs');

const TABLENAME = 'DeviceRegistry';

AWS.config.update({
    region: "ap-southeast-1a",
    endpoint: "http://localhost:8000"
});

var dynamodb = new AWS.DynamoDB();
var docClient = new AWS.DynamoDB.DocumentClient();

function ImportData() {
    console.log("Importing devices into DynamoDB Device Registry. Please wait...");

    var allDevices = JSON.parse(fs.readFileSync('deviceRegistry.json', 'utf8'));
    allDevices.forEach(function (device) {
        var params = {
            TableName: "DeviceRegistry",
            Item: {
                "deviceid": device.deviceid,
                "portalid": device.portalid,
                "nextSwitchOut": device.nextSwitchOut,
                "lastAccessed": device.lastAccessed,
                "friendlyName": device.friendlyName
            }
        };

        docClient.put(params, function (err, data) {
            if (err) {
                console.error("Unable to add device", device.deviceid, ". Error JSON:", JSON.stringify(err, null, 2));
                console.error("Skipping this device");
            } else {
                console.log("Device succeeded:", device.deviceid);
            }
        });
    });
}

function main() {
    console.log('Verifying Device Registry is already there...');
    dynamodb.listTables(function (err, data) {
        if (err) {
            console.error("Unable to list tables. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            if (data.TableNames.indexOf(TABLENAME) != -1) {
                console.log('Device Registry is there. Importing now...');
                ImportData();
            }
            else
                console.error('Table hasnt been create. Run CreateDeviceRegistry.js first');
        }
    });
}

main();