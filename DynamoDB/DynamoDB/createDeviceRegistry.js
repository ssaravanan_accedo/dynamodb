﻿var AWS = require("aws-sdk");

AWS.config.update({
    region: "ap-southeast-1a",
//    endpoint: "https://dynamodb.ap-southeast-1a.amazonaws.com"
    endpoint: "http://localhost:8000"
});

const TABLENAME = "DeviceRegistry";

var dynamodb = new AWS.DynamoDB();

// Table Schema
var params = {
    "TableName" : TABLENAME,
    "KeySchema": [       
        { "AttributeName": "deviceid", "KeyType": "HASH" },  //Partition key
        { "AttributeName": "lastAccessed", "KeyType": "RANGE" }  //Sort key
    ],
    "AttributeDefinitions": [       
        { "AttributeName": "deviceid", "AttributeType": "S" },
        { "AttributeName": "portalid", "AttributeType": "S" },
        { "AttributeName": "lastAccessed", "AttributeType": "S" }
    ],
    "GlobalSecondaryIndexes": [{
            "IndexName": "PortalID",
            "KeySchema": [
                { "AttributeName": "portalid", "KeyType": "HASH" } // Sort Key
            ],
            "Projection": {
                    "NonKeyAttributes": ["deviceid"],
                    "ProjectionType": "INCLUDE"
            },
            "ProvisionedThroughput": {
                "ReadCapacityUnits": 1, 
                "WriteCapacityUnits": 1
            }
        }
    ],
    "ProvisionedThroughput": {
        "ReadCapacityUnits": 2, 
        "WriteCapacityUnits": 2
    }
};

// Helper function to create a the device registry table
function CreateTable()
{
    dynamodb.createTable(params, function (err, data) {
        if (err) {
            console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("Device Registry created. Table description JSON:", JSON.stringify(data, null, 2));
        }
    });
}

function main()
{
    console.log('Verifying Device Registry hasnt be created before...');
    dynamodb.listTables(function (err, data) {
        if (err) {
            console.error("Unable to list table. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            if (data.TableNames.indexOf(TABLENAME) == -1) {
                console.log('Device Registry hasnt be created previously. Creating now...');
                CreateTable();
            }
        }
    });
}

main();