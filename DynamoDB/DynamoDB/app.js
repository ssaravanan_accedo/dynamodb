﻿var AWS = require("aws-sdk");

AWS.config.update({
    region: "ap-southeast-1a",
//    endpoint: "https://dynamodb.ap-southeast-1a.amazonaws.com"
    endpoint: "http://localhost:8000"
});

var dynamodb = new AWS.DynamoDB();

var params = {
    TableName : "Movies",
    KeySchema: [       
        { AttributeName: "device", KeyType: "HASH" },  //Partition key
        { AttributeName: "portalid", KeyType: "RANGE" }  //Sort key
    ],
    AttributeDefinitions: [       
        { AttributeName: "device", AttributeType: "S" },
        { AttributeName: "portalid", AttributeType: "S" }
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 2, 
        WriteCapacityUnits: 2
    }
};

function CreateTable_Movies()
{
    dynamodb.createTable(params, function (err, data) {
        if (err) {
            console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
        }
    });
}

function main()
{
    dynamodb.listTables(function (err, data) {
        if (err) {
            console.error("Unable to list table. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            if (!data.TableNames['Movies']) {
                console.log('Movies table not created. Creating now...');
                CreateTable_Movies();
            }
        }
    });
}

main();