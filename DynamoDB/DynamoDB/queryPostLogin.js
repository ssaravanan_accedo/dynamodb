﻿var AWS = require("aws-sdk");

AWS.config.update({
    region: "ap-southeast-1a",
    //    endpoint: "https://dynamodb.ap-southeast-1a.amazonaws.com"
    endpoint: "http://localhost:8000"
});

var docClient = new AWS.DynamoDB.DocumentClient();
var dynamodb = new AWS.DynamoDB();

const TABLENAME = "DeviceRegistry";

var params = {
    TableName: "DeviceRegistry",
    IndexName: "PortalID",
    ProjectionExpression: "#pid, deviceid, friendlyName",
    KeyConditionExpression: "#pid = :pidValue",
    ExpressionAttributeNames: {
        "#pid": "portalid"
    },
    ExpressionAttributeValues: {
        ":pidValue": "000001"
    }
};

function query() {
    docClient.query(params, function (err, data) {
        if (err) {
            console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
        } else {
            console.log("Query succeeded.");
            data.Items.forEach(function (item) {
                console.log(" -", item.deviceid + ": " + item.friendlyName);
            });
        }
    });
}

function main() {
    console.log('Verifying Device Registry is created...');
    dynamodb.listTables(function (err, data) {
        if (err) {
            console.error("Unable to list table. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            if (data.TableNames.indexOf(TABLENAME) != -1) {
                console.log('Device Registry has been created. Querying now...');
                query();
            }
            else
                console.log('Error: Table has not been created');
        }
    });
}

main();