# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
Quick demonstration on how to Create, Read and Update the DynamoDB. Note I've not included the sample for delete since its fairly easy. Pls reference Dynamo API reference here for the full info - http://docs.aws.amazon.com/amazondynamodb/latest/APIReference/Welcome.html
* Version 0.1

### How do I get set up? ###

* Install AWS CLI for you machine -
https://aws.amazon.com/cli/
* Setup Dynamo DB local storage - 
http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.htmlup
* Install AWS SDK for Node JS - 
https://aws.amazon.com/sdk-for-node-js/
* Dependencies - Node JS v4 LTS & NPM
* Database configuration
Configure the local endpoint - 
http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.Endpoint.html
* Running the scripts
*node createDeviceRegistry.js - creates the device registry table
*node loadData.js - loads the data from the deviceRegistry.json datastore
*node queryPostLogin.js - queries a secondary index (portal id) from the table
*node update.js - updates a 'record' in the DB

### Who do I talk to? ###

* Repo owner or admin
Saravanan @ Accedo